![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# VPFR QTGUI
This project is for controlling the vision pipeline via Rqt.  
Rqt is a graphical application of Ros that can be expanded with plugins.  
With this plugin, all information from the individual instances can be read from the vision pipeline and settings can also be written.  




Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/vpfr-qtgui/-/wikis/home)
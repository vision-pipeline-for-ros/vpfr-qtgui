"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

import os
import rospy
import rospkg
import rosservice
from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget
from python_qt_binding.QtWidgets import QMessageBox
from vpfr_qtgui.vpfr_instance import VpfrInstance
import re
from vpfr_qtgui.vpfrexception import VpfrException
from vpfr.msg import PerformanceMsg


class VpfrGui(Plugin):

    #
    # Init
    #

    def __init__(self, context):
        print("(vpfr-gui) init")
        super(VpfrGui, self).__init__(context)
        self.setObjectName("vpfr gui")
        self.__widget = QWidget()
        ui_file = os.path.join(
            rospkg.RosPack().get_path("vpfr_qtgui"), "resource", "vpfr_gui.ui"
        )
        loadUi(ui_file, self.__widget)
        self.__widget.setObjectName("vpfr gui")
        if context.serial_number() > 1:
            self.__widget.setWindowTitle(
                self.__widget.windowTitle()
                + (" (%d)" % context.serial_number())
            )
        context.add_widget(self.__widget)

        self.__rospack = rospkg.RosPack()
        self.__list_vpfr_instaces = []
        self.__place_holder_txt = ""
        self.__file_loa_regex = r"\w+\.yaml"
        self.__default_config_name = "default_config.yaml"

        self.__counter_perform = 0
        self.__selected_algorithm = None
        self.__plot_data = []
        self.__subscriber_performance = None
        self.__request_counter = 0
        # init
        self._init_gui()
        self._init_tab_algorithm()
        self._init_tab_options()
        self._init_tab_performance()
        self._update_data()

    def _init_gui(self):
        print("(vpfr-gui) init gui")
        self.__widget.ui_button_updatedata.clicked.connect(self._update_data)
        self.__widget.ui_vpfr_list.currentIndexChanged.connect(
            self._current_instance_change
        )

    def _init_tab_options(self):
        print("(vpfr-gui) init tab options")
        self.__widget.ui_button_sc_reset.clicked.connect(
            self._update_subscriber_textfield
        )
        self.__widget.ui_button_pc_reset.clicked.connect(
            self._update_publisher_textfield
        )
        self.__widget.ui_button_sc_set.clicked.connect(
            self._set_subscriber_channel
        )
        self.__widget.ui_button_pc_set.clicked.connect(
            self._set_publisher_channel
        )
        self.__widget.ui_textfield_pc.textEdited.connect(
            self._update_publisher_channel_button
        )
        self.__widget.ui_textfield_sc.textEdited.connect(
            self._update_subscriber_channel_button
        )
        self.__widget.ui_textfield_options_elfp.textEdited.connect(
            self._update_export_button
        )
        self.__widget.ui_button_options_elfp_clear.clicked.connect(
            self._update_export_button
        )
        self.__widget.ui_combo_config.currentIndexChanged.connect(
            self._loa_file_change
        )
        self.__widget.ui_button_ilftp_import.clicked.connect(
            self._import_loa_file
        )
        self.__widget.ui_button_options_elfp_export.clicked.connect(
            self._export_loa_file
        )
        self.__widget.ui_button_addata_set.clicked.connect(
            self._set_additional_data
        )

    def _init_tab_algorithm(self):
        print("(vpfr-gui) init tab algorithm")
        self.__widget.ui_button_scan_algo.clicked.connect(self._scan_algorithm)
        self.__widget.ui_button_reload_loa.clicked.connect(self._reload_loa)
        self.__widget.ui_list_algo.itemClicked.connect(self._select_algorithm)
        self.__widget.ui_list_algo.itemDoubleClicked.connect(
            self._set_algorithm
        )
        self.__widget.ui_button_set.clicked.connect(self._set_algorithm_config)
        self.__widget.ui_button_set_default_config.clicked.connect(
            self._set_default_algorithm_config
        )
        self.__widget.ui_config_list_algo.currentIndexChanged.connect(
            self._algorithm_config_change
        )

    def _init_tab_performance(self):
        print("(vpfr-gui) init tab performance")
        self.__data_plot = None
        try:
            from rqt_plot.data_plot import DataPlot

            self.__data_plot = DataPlot(self.__widget)
            self.__widget.ui_data_plot_tpr.addWidget(self.__data_plot)
            self.__widget.ui_group_tpr.setEnabled(True)
            self._clear_plot()
            self.__data_plot.add_curve(
                "tpr", "Time per Request [us]", [0], [0]
            )
            self.__data_plot.add_curve("cpu", "CPU load         [%]", [0], [0])
            self.__data_plot.add_curve("ram", "RAM consumption  [%]", [0], [0])
            self.__data_plot.redraw()
        except Exception:
            print("(vpfr-gui) can not load rqt_plot")
            self.__widget.ui_group_tpr.setEnabled(False)

    #
    # Update
    #

    def _callback_performance_data(self, data):
        self.__data_plot.update_values(
            "tpr", self.__counter_perform, data.time_per_request / 1000
        )
        self.__data_plot.update_values(
            "cpu", self.__counter_perform, data.cpu_usage
        )
        self.__data_plot.update_values(
            "ram", self.__counter_perform, data.ram_usage
        )
        self.__data_plot.redraw()
        self.__counter_perform = self.__counter_perform + 1

    def _show_alert(self, alert_type, message, message_info):
        msg = QMessageBox()
        msg.setIcon(alert_type)
        msg.setText(message)
        msg.setInformativeText(message_info)
        msg.setWindowTitle("VPFR-GUI Alert")
        msg.setDetailedText(
            "The details are as follows: \n"
            + self.__current_instance.get_vpfr_instance()
            + "\n"
            + self.__current_instance.get_vpfr_type()
        )
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def _export_loa_file(self):
        name = self.__widget.ui_textfield_options_elfp.text()
        print("(vpfr-gui) export loa file '" + name + "' from paramserver")
        try:
            self.__current_instance.export_loa_file(name)
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _update_export_button(self):
        if re.match(
            self.__file_loa_regex,
            self.__widget.ui_textfield_options_elfp.text(),
        ):
            self.__widget.ui_button_options_elfp_export.setEnabled(True)
        else:
            self.__widget.ui_button_options_elfp_export.setEnabled(False)

    def _set_algorithm_config(self):
        print("(vpfr-gui) set new algorithm config")
        try:
            self.__selected_algorithm.get_configs()[
                self.__widget.ui_config_list_algo.currentIndex()
            ].use_config()
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _set_default_algorithm_config(self):
        print("(vpfr-gui) set default algorithm config")
        for config in self.__selected_algorithm.get_configs():
            if config.get_name() == self.__default_config_name:
                try:
                    config.use_config()
                    self._update_instance()
                except rospy.ServiceException as exc:
                    print(
                        "(vpfr-gui) Service did not process request: "
                        + str(exc)
                    )
                    self._show_alert(
                        QMessageBox.Critical,
                        "Service did not process request",
                        str(exc),
                    )
                except VpfrException as exc:
                    print(
                        "(vpfr-gui) Service ('"
                        + exc.getPlace()
                        + "') call was unsuccessful because '"
                        + exc.getMessage()
                        + "'"
                    )
                    self._show_alert(
                        QMessageBox.Warning, exc.getMessage(), exc.getPlace()
                    )
                break

    def _clear_plot(self):
        if self.__data_plot is not None:
            self.__data_plot.clear_values()
            self.__data_plot.redraw()

    def _select_algorithm(self, item):
        self.__widget.ui_panel_algo_info.setEnabled(False)
        combo_box = self.__widget.ui_config_list_algo
        group = self.__widget.ui_group_algo_config
        group.setEnabled(False)
        combo_box.setEnabled(False)
        listalgo = self.__widget.ui_list_algo
        index = listalgo.row(item)
        i = 0
        sel_algorithm = None
        for algorithm in self.__current_instance.get_algorithms():
            if algorithm.is_enabled():
                if i == index:
                    sel_algorithm = algorithm
                    break
                i = i + 1
        self.__selected_algorithm = sel_algorithm

        print("(vpfr-gui) show algorithm ('" + sel_algorithm.get_name() + "')")

        self.__widget.ui_algo_info_name_value.setText(sel_algorithm.get_name())
        self.__widget.ui_algo_info_version_value.setText(
            sel_algorithm.get_version()
        )
        self.__widget.ui_algo_info_authors_value.setText(
            sel_algorithm.get_authors()
        )
        self.__widget.ui_algo_info_license_value.setText(
            sel_algorithm.get_license()
        )
        self.__widget.ui_algo_info_config_value.setText(
            sel_algorithm.get_current_config().get_name()
        )
        self.__widget.ui_algo_info_current_value.setText(
            str(sel_algorithm.is_current())
        )
        self.__widget.ui_algo_info_enabled_value.setText(
            str(sel_algorithm.is_enabled())
        )
        self.__widget.ui_algo_info_mainfile_value.setText(
            sel_algorithm.get_main_file()
        )
        self.__widget.ui_algo_info_mainclass_value.setText(
            sel_algorithm.get_main_class()
        )

        combo_box.clear()
        for config in sel_algorithm.get_configs():
            if config.is_current():
                combo_box.addItem(config.get_name() + " (Current)")
            else:
                combo_box.addItem(config.get_name())
        group.setEnabled(True)
        combo_box.setEnabled(True)
        self.__widget.ui_panel_algo_info.setEnabled(True)

    def _set_algorithm(self, item):
        listalgo = self.__widget.ui_list_algo
        index = listalgo.row(item)
        i = 0
        sel_algorithm = None
        for algorithm in self.__current_instance.get_algorithms():
            if algorithm.is_enabled():
                if i == index:
                    sel_algorithm = algorithm
                    break
                i = i + 1
        print("(vpfr-gui) set algorithm ('" + sel_algorithm.get_name() + "')")
        if not sel_algorithm.is_current():
            try:
                sel_algorithm.set_current()
                self._update_instance()
            except rospy.ServiceException as exc:
                print(
                    "(vpfr-gui) Service did not process request: " + str(exc)
                )
                self._show_alert(
                    QMessageBox.Critical,
                    "Service did not process request",
                    str(exc),
                )
                self._update_data()
            except VpfrException as exc:
                print(
                    "(vpfr-gui) Service ('"
                    + exc.getPlace()
                    + "') call was unsuccessful because '"
                    + exc.getMessage()
                    + "'"
                )
                self._show_alert(
                    QMessageBox.Warning, exc.getMessage(), exc.getPlace()
                )

    def _update_subscriber_channel_button(self):
        if (
            self.__widget.ui_textfield_sc.text()
            == self.__current_instance.get_subscriber_channel()
        ):
            self.__widget.ui_button_sc_set.setEnabled(False)
            self.__widget.ui_button_sc_reset.setEnabled(False)
        else:
            self.__widget.ui_button_sc_set.setEnabled(True)
            self.__widget.ui_button_sc_reset.setEnabled(True)

    def _update_publisher_channel_button(self):
        if (
            self.__widget.ui_textfield_pc.text()
            == self.__current_instance.get_publisher_channel()
        ):
            self.__widget.ui_button_pc_reset.setEnabled(False)
            self.__widget.ui_button_pc_set.setEnabled(False)
        else:
            self.__widget.ui_button_pc_set.setEnabled(True)
            self.__widget.ui_button_pc_reset.setEnabled(True)

    def _update_data(self):
        print("(vpfr-gui) update data")
        self._disable_tabs()
        self.__list_vpfr_instaces = self._scan_vpfr_instances()
        self._update_vpfr_combo_box()
        self._current_instance_change(
            self.__widget.ui_vpfr_list.currentIndex()
        )

    def _current_instance_change(self, index):
        print("(vpfr-gui) change vpfr instance to index: " + str(index))
        if index == -1:
            self.__current_instance = None
        else:
            self.__current_instance = self.__list_vpfr_instaces[index]
        self._update_instance()

    def _update_instance(self):
        print("(vpfr-gui) update instance")
        self._clear_tabs()
        if self.__current_instance is not None:
            try:
                self.__current_instance.update()
                self._update_tabs()
            except rospy.ServiceException as exc:
                print(
                    "(vpfr-gui) Service did not process request: " + str(exc)
                )
                self._show_alert(
                    QMessageBox.Critical,
                    "Service did not process request",
                    str(exc),
                )
            except VpfrException as exc:
                print(
                    "(vpfr-gui) Service ('"
                    + exc.getPlace()
                    + "') call was unsuccessful because '"
                    + exc.getMessage()
                    + "'"
                )
                self._show_alert(
                    QMessageBox.Warning, exc.getMessage(), exc.getPlace()
                )

    def _update_vpfr_combo_box(self):
        combo_box = self.__widget.ui_vpfr_list
        combo_box.setEnabled(False)
        combo_box.clear()
        for instace in self.__list_vpfr_instaces:
            combo_box.addItem(
                instace.get_vpfr_type() + "/" + instace.get_vpfr_instance()
            )
        if len(self.__list_vpfr_instaces) > 0:
            combo_box.setEnabled(True)

    def _update_publisher_textfield(self):
        print("(vpfr-gui) reset publisher textfield")
        self.__widget.ui_textfield_pc.setText(
            self.__current_instance.get_publisher_channel()
        )
        self._update_publisher_channel_button()

    def _update_subscriber_textfield(self):
        print("(vpfr-gui) reset subscriber textfield")
        self.__widget.ui_textfield_sc.setText(
            self.__current_instance.get_subscriber_channel()
        )
        self._update_subscriber_channel_button()

    def _update_loa_combo_box(self):
        combo_box = self.__widget.ui_combo_config
        group = self.__widget.ui_group_ilftp
        group.setEnabled(False)
        combo_box.setEnabled(False)
        combo_box.clear()
        for loa_file_name in self.__current_instance.get_available_loa_files():
            combo_box.addItem(loa_file_name.get_name())
        if len(self.__current_instance.get_available_loa_files()) > 0:
            self._loa_file_change()
            group.setEnabled(True)
            combo_box.setEnabled(True)

    def _set_additional_data(self):
        ad_buffer = self.__widget.ui_textfield_addata.toPlainText()
        ad_data = ad_buffer.split()
        try:
            self.__current_instance.set_additional_data(ad_data)
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    # Tabs

    def _update_tabs(self):
        print("(vpfr-gui) update widget")
        if self.__current_instance is not None:
            tabs = self.__widget.ui_tabs
            tabs.setEnabled(False)
            self._update_tab_info()
            self._update_tab_algorithms()
            self._update_tab_options()
            self._update_tab_performance()
            tabs.setEnabled(True)

    def _update_tab_info(self):
        print("(vpfr-gui) update tab info")
        # vpfr data
        self.__widget.ui_label_vpfrdata_instancename_value.setText(
            self.__current_instance.get_vpfr_instance()
        )
        self.__widget.ui_label_vpfrdata_typepackage_value.setText(
            self.__current_instance.get_vpfr_type()
        )
        self.__widget.ui_label_vpfrdata_version_value.setText(
            self._get_package_version("vpfr")
        )
        self.__widget.ui_label_vpfrdata_typeversion_value.setText(
            self._get_package_version(self.__current_instance.get_vpfr_type())
        )
        # publisher and subscriber channel
        self.__widget.ui_label_pasc_pub_value.setText(
            self.__current_instance.get_publisher_channel()
        )
        self.__widget.ui_label_pasc_pubtype_value.setText(
            self.__current_instance.get_publisher_channel_type()
        )
        self.__widget.ui_label_pasc_subtype_value.setText(
            self.__current_instance.get_subscriber_channel_type()
        )
        self.__widget.ui_label_pasc_sub_value.setText(
            self.__current_instance.get_subscriber_channel()
        )
        # current algorithm
        algorithm = self.__current_instance.get_current_algorithm()
        if algorithm is not None:
            self.__widget.ui_label_ca_name_value.setText(algorithm.get_name())
            self.__widget.ui_label_ca_version_value.setText(
                algorithm.get_version()
            )
            self.__widget.ui_label_ca_config_value.setText(
                algorithm.get_current_config().get_name()
            )
            self.__widget.ui_group_ca.setEnabled(True)
        # other
        self.__widget.ui_label_other_algocount_value.setText(
            str(len(self.__current_instance.get_algorithms()))
        )
        i = 0
        for a in self.__current_instance.get_algorithms():
            if a.is_enabled():
                i = i + 1
        self.__widget.ui_label_other_algoencount_value.setText(str(i))

    def _update_tab_options(self):
        print("(vpfr-gui) update tab options")
        # publisher and subscriber channel
        self._update_publisher_textfield()
        self._update_subscriber_textfield()

        ad_buffer = ""
        for line in self.__current_instance.get_additional_data():
            ad_buffer = ad_buffer + line + "\n"
        self.__widget.ui_textfield_addata.setText(ad_buffer)

        self._update_loa_combo_box()

    def _update_tab_algorithms(self):
        print("(vpfr-gui) update tab algorithms")
        listalgo = self.__widget.ui_list_algo
        for algorithm in self.__current_instance.get_algorithms():
            if algorithm.is_enabled():
                if algorithm.is_current():
                    listalgo.addItem(algorithm.get_name() + " (Current)")
                else:
                    listalgo.addItem(algorithm.get_name())

    def _update_tab_performance(self):
        print("(vpfr-gui) update tab performance")
        self._clear_plot()
        if self.__subscriber_performance is not None:
            self.__subscriber_performance.unregister()
        if self.__data_plot is not None:
            self.__subscriber_performance = rospy.Subscriber(
                self.__current_instance.get_service_pre_address() + "analytic",
                PerformanceMsg,
                self._callback_performance_data,
            )

    #
    # Clear
    #

    def _clear_tabs(self):
        self._request_counter = 0
        self._clear_tab_info()
        self._clear_tab_algorithms()

    def _clear_tab_info(self):
        self.__widget.ui_label_ca_name_value.setText(self.__place_holder_txt)
        self.__widget.ui_label_ca_version_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_label_ca_config_value.setText(self.__place_holder_txt)
        self.__widget.ui_group_ca.setEnabled(False)

    def _clear_tab_algorithms(self):
        self.__widget.ui_list_algo.clear()
        self.__selected_algorithm = None
        self.__widget.ui_panel_algo_info.setEnabled(False)
        self.__widget.ui_config_list_algo.setEnabled(False)
        self.__widget.ui_group_algo_config.setEnabled(False)
        self.__widget.ui_config_list_algo.clear()
        self.__widget.ui_algo_info_name_value.setText(self.__place_holder_txt)
        self.__widget.ui_algo_info_version_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_authors_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_license_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_current_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_enabled_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_config_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_mainfile_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_info_mainclass_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_config_path_value.setText(
            self.__place_holder_txt
        )
        self.__widget.ui_algo_config_current_value.setText(
            self.__place_holder_txt
        )

    #
    # Other
    #

    def _set_subscriber_channel(self):
        print("(vpfr-gui) set subscriber channel")
        try:
            self.__current_instance.set_subscriber_channel(
                self.__widget.ui_textfield_sc.text()
            )
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _set_publisher_channel(self):
        print("(vpfr-gui) set publisher channel")
        try:
            self.__current_instance.set_publisher_channel(
                self.__widget.ui_textfield_pc.text()
            )
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _get_package_version(self, pack):
        try:
            manifest = self.__rospack.get_manifest(pack)
            return manifest.version
        except Exception:
            return None

    def _scan_vpfr_instances(self):
        list_of_vpfr = []
        service_list = rosservice.get_service_list()
        for service in service_list:
            if service.startswith("/vpfr/"):  # scan all vpfr services
                vpfr_service_data = service.split("/")
                vpfr_type = vpfr_service_data[2]  # type name
                vpfr_instance = vpfr_service_data[3]  # instace name
                found = False
                for check_instance in list_of_vpfr:
                    if (
                        check_instance.get_vpfr_type() == vpfr_type
                        and check_instance.get_vpfr_instance() == vpfr_instance
                    ):
                        found = True
                        break
                if not found:
                    list_of_vpfr.append(VpfrInstance(vpfr_type, vpfr_instance))
        return list_of_vpfr

    def _scan_algorithm(self):
        print("(vpfr-gui) scan new algorithm")
        try:
            self.__current_instance.scan_algorithm()
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _reload_loa(self):
        print("(vpfr-gui) reload loa")
        try:
            self.__current_instance.reload_loa()
            self._update_instance()
        except rospy.ServiceException as exc:
            print("(vpfr-gui) Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "(vpfr-gui) Service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _import_loa_file(self):
        print("(vpfr-gui) import loa file to paramserver")
        try:
            self.__current_instance.get_available_loa_files()[
                self.__widget.ui_combo_config.currentIndex()
            ].import_loa_file()
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
            self._show_alert(
                QMessageBox.Critical,
                "Service did not process request",
                str(exc),
            )
            self._update_data()
        except VpfrException as exc:
            print(
                "service ('"
                + exc.getPlace()
                + "') call was unsuccessful because '"
                + exc.getMessage()
                + "'"
            )
            self._show_alert(
                QMessageBox.Warning, exc.getMessage(), exc.getPlace()
            )

    def _loa_file_change(self):
        if self.__widget.ui_combo_config.currentIndex() >= 0:
            self.__widget.ui_iloa_path_value.setText(
                self.__current_instance.get_available_loa_files()[
                    self.__widget.ui_combo_config.currentIndex()
                ].get_path()
            )

    def _algorithm_config_change(self):
        if self.__selected_algorithm is not None:
            algo = self.__selected_algorithm.get_configs()[
                self.__widget.ui_config_list_algo.currentIndex()
            ]
            self.__widget.ui_algo_config_path_value.setText(algo.get_path())
            self.__widget.ui_algo_config_current_value.setText(
                str(algo.is_current())
            )

    def _disable_tabs(self):
        tabs = self.__widget.ui_tabs
        tabs.setEnabled(False)

    def _shutdown_plugin(self):
        if self.__subscriber_performance is not None:
            self.__subscriber_performance.unregister()
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # v = instance_settings.value(k)
        pass

    # def trigger_configuration(self):
    # Comment in to signal that the plugin has a way to configure
    # This will enable a setting button (gear icon) in each dock widget title bar
    # Usually used to open a modal configuration dialog

"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

from vpfr.srv import *
import rospy
from vpfr_qtgui.vpfrexception import VpfrException


class LoaFile:
    def __init__(self, instance, loa_name, loa_path):
        self.__instance = instance
        self.__loa_name = loa_name
        self.__loa_path = loa_path

    def get_name(self):
        return self.__loa_name

    def get_path(self):
        return self.__loa_path

    def import_loa_file(self):
        copy_loa_from_config_to_paramserver = rospy.ServiceProxy(
            self.__instance.get_service_pre_address()
            + "copy_loa_from_config_to_paramserver",
            ConfigLoaSet,
        )
        data = copy_loa_from_config_to_paramserver(self.__loa_name)
        if not data.success:
            raise VpfrException(
                self.__instance.get_service_pre_address()
                + "copy_loa_from_config_to_paramserver",
                data.message,
            )

"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""
from vpfr.srv import *
from vpfr.msg import *
import rospy
from vpfr_qtgui.loafile import LoaFile
from vpfr_qtgui.algorithm import Algorithm
from vpfr_qtgui.config import AlgorithmConfig
from vpfr_qtgui.vpfrexception import VpfrException


class VpfrInstance:
    def __init__(self, vpfr_type, vpfr_instance):
        self.__vpfr_type = vpfr_type
        self.__vpfr_instance = vpfr_instance
        self.__service_pre_address = (
            "/vpfr/" + self.__vpfr_type + "/" + self.__vpfr_instance + "/"
        )
        self.__subscribe_channel = None
        self.__publish_channel = None
        self.__publish_channel_type = None
        self.__subscribe_channel_type = None
        self.__loa_files = []
        self.__algorithms = []
        self.__add_ata = []

    def get_vpfr_instance(self):
        return self.__vpfr_instance

    def get_vpfr_type(self):
        return self.__vpfr_type

    def get_service_pre_address(self):
        return self.__service_pre_address

    def update(self):
        # getSubscriberPublisherChannel
        get_current_channels = rospy.ServiceProxy(
            self.get_service_pre_address() + "get_current_channels",
            ChannelRequest,
        )
        data_gcc = get_current_channels()
        if data_gcc.success:
            self.__subscribe_channel = data_gcc.subscribe_channel
            self.__publish_channel = data_gcc.publish_channel
            self.__publish_channel_type = data_gcc.publish_channel_type
            self.__subscrib_channel_type = data_gcc.subscribe_channel_type
        else:
            raise VpfrException(
                self.get_service_pre_address() + "get_current_channels",
                data_gcc.message,
            )

        # getAvailableloa_files
        self.__loa_files = []
        get_loa_files = rospy.ServiceProxy(
            self.get_service_pre_address() + "get_loa_files", ConfigLoaList
        )
        data_loa_files = get_loa_files()
        if data_loa_files.success:
            for loa_msg in data_loa_files.available_loas:
                self.__loa_files.append(
                    LoaFile(self, loa_msg.name, loa_msg.path)
                )
        else:
            raise VpfrException(
                self.get_service_pre_address() + "get_loa_files",
                data_loa_files.message,
            )

        # getAdditionalData
        self.__add_ata = []
        get_additional_data = rospy.ServiceProxy(
            self.get_service_pre_address() + "get_additional_data",
            AdditionalDataRequest,
        )
        data_ad = get_additional_data()
        if data_ad.success:
            self.__add_ata = data_ad.value
        else:
            raise VpfrException(
                self.get_service_pre_address() + "get_additional_data",
                data_ad.message,
            )

        # getAlgorithms
        self.__algorithms = []
        get_algorithms = rospy.ServiceProxy(
            self.get_service_pre_address() + "get_algorithms",
            AlgorithmInfoList,
        )
        data_algorithms = get_algorithms()
        if data_algorithms.success:
            for algo_vpfr in data_algorithms.algorithms:
                configs = []
                for config_vpfr in algo_vpfr.configs:
                    configs.append(
                        AlgorithmConfig(
                            self,
                            algo_vpfr.name,
                            config_vpfr.name,
                            config_vpfr.path,
                            config_vpfr.current,
                        )
                    )
                self.__algorithms.append(
                    Algorithm(
                        self,
                        algo_vpfr.name,
                        algo_vpfr.version,
                        algo_vpfr.authors,
                        algo_vpfr.license,
                        algo_vpfr.main_class,
                        algo_vpfr.main_file,
                        algo_vpfr.loader,
                        configs,
                        algo_vpfr.enabled,
                        algo_vpfr.current,
                    )
                )
        else:
            raise VpfrException(
                self.get_service_pre_address() + "get_algorithms",
                data_algorithms.message,
            )

    def get_subscriber_channel(self):
        return self.__subscribe_channel

    def get_publisher_channel(self):
        return self.__publish_channel

    def get_publisher_channel_type(self):
        return self.__publish_channel_type

    def get_subscriber_channel_type(self):
        return self.__subscrib_channel_type

    def get_subscriber_channel(self):
        return self.__subscribe_channel

    def get_available_loa_files(self):
        return self.__loa_files

    def get_algorithms(self):
        return self.__algorithms

    def get_additional_data(self):
        return self.__add_ata

    def get_current_algorithm(self):
        for algo in self.__algorithms:
            if algo.is_current():
                return algo
        return None

    def scan_algorithm(self):
        scan_algorithms = rospy.ServiceProxy(
            self.get_service_pre_address() + "scan_algorithms", Request
        )
        data = scan_algorithms()
        if not data.success:
            raise VpfrException(
                self.get_service_pre_address() + "scan_algorithms",
                data.message,
            )

    def reload_loa(self):
        reload_loa = rospy.ServiceProxy(
            self.get_service_pre_address() + "reload_loa", Request
        )
        data = reload_loa()
        if not data.success:
            raise VpfrException(
                self.get_service_pre_address() + "reload_loa", data.message
            )

    def export_loa_file(self, name):
        copy_loa_from_paramserver_to_config = rospy.ServiceProxy(
            self.get_service_pre_address()
            + "copy_loa_from_paramserver_to_config",
            ConfigLoaSet,
        )
        data = copy_loa_from_paramserver_to_config(name)
        if not data.success:
            raise VpfrException(
                self.get_service_pre_address()
                + "copy_loa_from_paramserver_to_config",
                data.message,
            )

    def set_publisher_channel(self, channel):
        set_publisher = rospy.ServiceProxy(
            self.get_service_pre_address() + "set_publisher", ChannelSet
        )
        data = set_publisher(channel)
        if not data.success:
            raise VpfrException(
                self.get_service_pre_address() + "set_publisher", data.message
            )

    def set_additional_data(self, array_add_data):
        set_additional_data = rospy.ServiceProxy(
            self.get_service_pre_address() + "set_additional_data",
            AdditionalDataSet,
        )
        data = set_additional_data(array_add_data)
        if not data.success:
            raise VpfrException(
                self.get_service_pre_address() + "set_additional_data",
                data.message,
            )

    def set_subscriber_channel(self, channel):
        set_subscriber = rospy.ServiceProxy(
            self.get_service_pre_address() + "set_subscriber", ChannelSet
        )
        data = set_subscriber(channel)
        if not data.success:
            raise VpfrException(
                self.get_service_pre_address() + "set_subscriber", data.message
            )

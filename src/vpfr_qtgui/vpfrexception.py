"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""


class VpfrException(Exception):
    def __init__(self, place, message):
        self.__place = place
        self.__message = message

    def getMessage(self):
        return self.__message

    def getPlace(self):
        return self.__place
